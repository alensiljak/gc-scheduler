# gc-mobile-schedule

GnuCash mobile scheduled transactions handler

The idea here is to create a mobile app that would handle GnuCash Scheduled Transactions:

- importing them from the original GC book,
    - allow synchronizing the scheduled transactions list?
- alerting the user about transactions due,
- allowing the preview of the transactions schedule,
- generating .QIF files for the real transactions, to be entered to GnuCash
    - the generated transactions can be modified before the export

The app is to be developed using NativeScript, to allow running on multiple mobile operating systems.
