"""
A custom alhorhythm to calculate the due date for a scheduled transaction.
# adv_notify
# end_date
# last_occur
# name
# recurrence
# start_date

# template_account
# template_act_guid

Recurrence
recurrence_mult
recurrence_period_start
recurrence_period_type
recurrence_weekend_adjust
"""
from gnucash_portfolio import BookAggregate
from piecash import ScheduledTransaction

book = BookAggregate()

# Get the guinea-pig transaction to work on
tx: ScheduledTransaction = book.scheduled.get_enabled()[0]
print(f"Working with {tx}")

def get_scheduled_due_date(tx: ScheduledTransaction):
    """
    Identify the earliest due date 
    """
    last_instance_date = tx.last_occur
    if tx.recurrence.recurrence_period_start > last_instance_date:
        return tx.recurrence.recurrence_period_start
    else:
        return last_instance_date

def get_due_date(tx: ScheduledTransaction):
    """
    Calculate the next due date
    This is the date when the next instance should occur.
    """
    from pydatum import Datum

    # Take the last instance date / start date.
    scheduled_due_date = get_scheduled_due_date(tx)
    print(f"scheduled due date: {scheduled_due_date}")
    
    # todo: are we due?
    today: Datum = Datum()
    if scheduled_due_date < today.date:
        # due
        return scheduled_due_date
    else:
        return scheduled_due_date

#print(tx.recurrence)
#tx.recurrence.

result = get_due_date(tx)
print (result)