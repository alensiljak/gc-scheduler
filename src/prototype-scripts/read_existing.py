"""
Read scheduled transactions from GnuCash 
"""
from gnucash_portfolio import BookAggregate

book = BookAggregate()
txs = book.scheduled.get_all()
for tx in txs:
    print(f"{tx}")
