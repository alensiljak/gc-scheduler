# GC Mobile Schedule

This is the implementation in BeeWare Toga.

GnuCash mobile schedule

## Setup

Create a virtual environment

on Windows:

```
py -m venv venv
venv\Scripts\activate.bat
```

## Running

Execute `python -m gcmobileschedule` from this directory.

## Packaging 

Install Briefcase and run `python setup.py <platform> -s`, where platform is windows, linux, android, etc.

### Android

The default template requires some adjustments at this stage:

- Create 3.7 branch for the template
- get support tools jar, see https://github.com/pybee/Python-Android-support
- update destination SDK versions

