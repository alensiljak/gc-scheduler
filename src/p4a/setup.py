from distutils.core import setup
from setuptools import find_packages

options = {'apk': {'debug': None,
                   'requirements': 'python3,flask',
                   'android-api': 27,
                   'ndk-dir': '/home/alen/lib/android-ndk-r18b',
                   'dist-name': 'gcmobileschedule',
                   'ndk-version': '18b',
                   'bootstrap': 'webview',
                   'permissions': ['INTERNET', 'VIBRATE'],
                   'arch': 'armeabi-v7a',
                   'window': None,
                   }}

package_data = {'': ['*.py',
                     '*.png']
                }

packages = find_packages()
print('packages are', packages)

setup(
    name='gcmobileschedule',
    version='1.0',
    description='p4a flask',
    author='Alen Siljak',
    author_email='alen.siljak@gmx.com',
    packages=find_packages(),
    options=options,
    package_data={'gcmobileschedule': ['*.py', '*.png'],
                  'gcmobileschedule/static': ['*.png', '*.css'],
                  'gcmobileschedule/templates': ['*.html']}
)