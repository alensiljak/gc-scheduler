# GnuCash Mobile Scheduler

Implementation using [python-for-android](https://python-for-android.readthedocs.io). 

## Development Environment

p4a currently works only on Linux.

## Build

Install the build dependencies:

- cython, via pip, v0.21
- javac (openjdk-devel)
- g++ (gcc-c++)
- libtool
- ccache, optional

### Buildozer

Create buildozer.spec file with `buildozer init` and adjust the details.

Buildozer fix: `export USE_SDK_WRAPPER=True`

`buildozer -v android debug`

When changing buildozer.spec, run `buildozer android update`.

References:

- https://buildozer.readthedocs.io/en/latest/quickstart.html#init-and-build-for-android
 
### p4a

The recommendation is to use buildozer. It uses p4a in the background.

Set the variables:

```
export ANDROIDSDK="$HOME/lib/android-sdk"
export ANDROIDAPI="27"

export ANDROIDNDK="$HOME/lib/android-ndk-r18b"
export NDKAPI="19"
export ANDROIDNDKVER="r18b"
```

Webview

```
p4a apk --private $HOME/src/gc-mobile-schedule/src/p4a --package=ml.alensiljak.gcmobileschedule --name "GnuCash Mobile Schedule" --version 0.1 --bootstrap=webview --port=5000 --arch=armeabi-v7a --sdk-dir $HOME/lib/android-sdk --android_api 27 --ndk-dir $HOME/lib/android-ndk-r18b --ndk_version 18b
```
bootstrap=webview,sdl2
--requirements=flask,python3crystax,python3

References

- [Discord channels](https://discordapp.com/channels/423249981340778496/423250272316293120)

### Python2

The compilation for Python2 also fails.

requirements=python2,flask,pyjnius

Not necessary but can try setting:

```
export ANDROID_SDK_ROOT="$HOME/lib/android-sdk"
export ANDROID_NDK_ROOT="$HOME/lib/android-ndk-r18b"
```

## Problems

Missing Python.h, [question](https://stackoverflow.com/questions/21530577/fatal-error-python-h-no-such-file-or-directory). 
Install python3-devel package.
